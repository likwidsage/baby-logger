import React from 'react'
import { Route, Switch } from 'react-router-dom'

import Home from './Home'
import Image from './Image'

function MainRouter() {
  return (
    <>
      <Route path='/' component={Home} />
      <Route path='/image/:id' component={Image} />
    </>
  )
}

export default MainRouter
