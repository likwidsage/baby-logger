import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import { useParams, useHistory } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles';
import { Close } from '@material-ui/icons';
import { Box, IconButton } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  image: {
    position: 'fixed',
    left: 20,
    top: 20,
    bottom: 20,
    right: 20,
    background: '#fff',
  },
}))

function Test() {
  const { id } = useParams()
  const [url, SetUrl] = useState('')
  const classes = useStyles()
  const history = useHistory();

  useEffect(() => {
    Axios.get(`/api/entries/image/${id}`)
      .then(res => {
        const image = res.data
        console.log(res.data)
        SetUrl(`/image/${image.userid}/${image.fileName}`)
      })
  }, [id])

  useEffect(() => {
    console.log('URL:', url)
  }, [url])

  return (
    <Box className={classes.image}>
      <Box display='flex' alignItems='center'>
        <Box>
          <IconButton onClick={() => history.push('/')} >
            <Close />
          </IconButton>
        </Box>
        <Box>
          {url}
        </Box>
      </Box>
      <div style={{
        width: '100%',
        height: '100%',
        backgroundImage: `url(${url})`,
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
      }} >
      </div>
    </Box>
  )
}

export default Test