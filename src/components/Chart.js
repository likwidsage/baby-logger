import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import { ResponsiveLine } from '@nivo/line'
import { Paper } from '@material-ui/core'


const useStyles = makeStyles(theme => ({
  fab: {
    bottom: theme.spacing(1),
    right: theme.spacing(1),
    position: 'fixed'
  },
  paper: {
    margin: 8,
    
  },
  tableCell: {
    padding: 10,    
  },
  chart: {
    height: 300,
    margin: 8,
  },
  Appbar: {
    padding: 8
  }
}))

const Chart = () => {
  const entries = useSelector(state => state.entries.list)
  const classes = useStyles()
  const [Data, setData] = useState(
    [
      {
        id: 'Feeding Chart',
        color: 'hsl(156, 70%, 50%)',
        data: [{ x: 0, y: 0 }]
      }
    ]
  )

  useEffect(() => {
    const newData = entries
      .filter(entry => entry.bottleAmount > 0)
      .map(entry => {
        const formattedTime = new Date(entry.time).toLocaleString('en-US', { timeZone: 'America/New_York', 
          month: '2-digit',
          day: '2-digit',
          hour: '2-digit',
          minute: '2-digit' })

        return {
          x: formattedTime,
          y: entry.bottleAmount
        }
      })
      .slice(0,15)
      .reverse()

    setData(Data => [{...Data[0], data: newData}])
  }, [entries])

  return (
    <Paper className={classes.chart}>
      <ResponsiveLine
        data={Data}
        margin={{ top: 50, right: 50, bottom: 50, left: 50 }}
        curve='natural'
        enableArea={true}
        pointSize={10}
        useMesh
      />
    </Paper>
  )
}

export default Chart