import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import Axios from 'axios'

import { makeStyles } from '@material-ui/core/styles'
import { 
  FormControlLabel,
  Grid,
  Button,
  TextField,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@material-ui/core'
import { DateTimePicker } from '@material-ui/pickers'

import { useDispatch, useSelector } from 'react-redux'
import { SetEntries } from '../../actions/entriesActions'

import Loader from 'react-spinners/SyncLoader'

const useStyles = makeStyles({
  rows: {
    marginBottom: 24
  },
  textField : {
    marginTop: 8,
    marginRight: 8,
  },
  text: {
    wordBreak: 'break-all'
  }
})

// TODO get userID from login
const userId = 1

const EntryDialog = props => {
  const classes = useStyles()
  const { fnClose, isOpen, editId } = props
  const dispatch = useDispatch()
  const entries = useSelector(store => store.entries.list)

  const defaultObj = { 
    userId,
    time: new Date(),
    diaperContents: {
      pee: false,
      poop: false,
      image: undefined
    },
    wasBreastFed: false,
    breastAmounts : {
      left: undefined,
      right: undefined
    },
    wasBottleFed: false,
  }
  
  const [formInfo, setFormInfo] = useState(defaultObj)
  const [IsSubmitting, SetIsSubmitting] = useState(false)

  const handleSubmit = () => {
    SetIsSubmitting(true)
    Axios.post('/api/entries/', formInfo)
      .then((res) => {
        console.log('added: ', res.data)
        Axios.get('/api/entries')
          .then(res => {
            dispatch(SetEntries(res.data))
            fnClose()
            SetIsSubmitting(false)
          })
          .catch(err => {
            console.error(err)
            SetIsSubmitting(false)
          })
      })
  }

  const handleChange = effectedObj => {
    setFormInfo({ ...formInfo, ...effectedObj })
  }

  const handleImageUpload = (e) => {
    e.preventDefault()
    console.log('image added', e.target.files)

    let imageFormObject = new FormData()
    imageFormObject.append('imageData', e.target.files[0])
    imageFormObject.append('imageName', e.target.files[0].name)

    Axios.post('/api/entries/image', imageFormObject)
      .then(data => {
        let {image} = data.data
        console.dir(image)
        setFormInfo({...formInfo, diaperContents: {...formInfo.diaperContents, image: image._id}})
      })
      .catch(err => {
        console.error(err)
      })
  }

  const handleImageRemove = e => {
    e.preventDefault()
    Axios.delete('/api/entries/image', {data : {image: formInfo.diaperContents.image}})
      .then(() => {
        setFormInfo(i => ({...i, diaperContents: {...i.diaperContents, image: undefined}}))
      })
  }
  
  const handleClose = () => {
    fnClose()
  }

  useEffect(() => {
    if(editId) { 
      const editing = entries.filter(entry => entry._id === editId)[0]
      setFormInfo(editing)
    }
  }, [editId,entries])

  useEffect(() => {
    if (isOpen && !editId){
      setFormInfo(info => ({...info, time: new Date()}))
    }
  }, [isOpen, editId])

  // useEffect(() => {
  //   console.log(formInfo)
  // }, [formInfo])


  return (
    <Dialog open={isOpen} aria-labelledby="form-dialog-title">
      <DialogTitle id="simple-dialog-title">Add entry</DialogTitle>
      <DialogContent>
        <form action="/api/entries" method='post' onSubmit={handleSubmit} encType="multipart/form-data" >
          <Grid container alignItems='flex-start'>
            <Grid item xs={3}  className={classes.rows}>
            Time
            </Grid>
            <Grid item xs={9}  className={classes.rows}>
              <DateTimePicker value={formInfo.time || new Date()} onChange={e => {
                handleChange({time: e})
              }} />
            </Grid>

            <Grid item xs={3}  className={classes.rows}>
            Diaper
            </Grid>
            <Grid item xs={9}  className={classes.rows}>
              <Grid container>
                <Grid item xs={4} >
                  <FormControlLabel control={
                    <Checkbox
                      checked={formInfo.diaperContents.poop || false}
                      onChange={e => handleChange({ diaperContents: { ...formInfo.diaperContents, poop: e.target.checked } })}
                    />
                  } label="Poop" />
                </Grid>
                <Grid item xs={4} >
                  <FormControlLabel control={
                    <Checkbox
                      checked={formInfo.diaperContents.pee || false}
                      onChange={e => handleChange({ diaperContents: { ...formInfo.diaperContents, pee: e.target.checked } })}
                    />
                  } label="Pee" />
                </Grid>
                <Grid item xs={4} >
                  
                  {formInfo.diaperContents.image ? (
                    <Button variant="outlined" component="span" className={classes.button} 
                      onClick={handleImageRemove}
                    >
                        Remove
                    </Button>
                  ): (
                    <div>
                      <input
                        accept="image/*"
                        className={classes.input}
                        style={{ display: 'none' }}
                        id="button-file"
                        type="file"
                        name="DiaperImage"
                        onChange={handleImageUpload}
                      />
                      <label htmlFor="button-file">
                        <Button variant="outlined" component="span" className={classes.button}>
                          add image
                        </Button>
                      </label> 
                    </div>
                  ) }
                </Grid>
              </Grid>
              <Grid item xs={4} >
              </Grid>
            </Grid>

            <Grid item xs={3}  className={classes.rows}>
            Feeding
            </Grid>
            <Grid item xs={9}  className={classes.rows}>
              <Grid container>
                <Grid item xs={6}>
                  <FormControlLabel control={
                    <Checkbox
                      checked={formInfo.wasBottleFed || false}
                      onChange={e => handleChange({ wasBottleFed: e.target.checked })}
                    />
                  } label="Bottle" />
                  {formInfo.wasBottleFed ? (
                    <Grid item xs={12}>
                      <TextField 
                        variant='outlined'
                        label='Amount'
                        onChange={e => handleChange({ bottleAmount: e.target.value })}
                        className={classes.textField}
                        value={formInfo.bottleAmount}
                        type="number"
                      />
                    </Grid>
                  ) : null}
                </Grid>
                <Grid item xs={6}>
                  <FormControlLabel control={
                    <Checkbox
                      checked={formInfo.wasBreastFed || false}
                      onChange={e => handleChange({ wasBreastFed: e.target.checked })}
                    />
                  } label="Breast" />
                  {formInfo.wasBreastFed ? (
                    <div>
                      <Grid item xs={12}>
                        <TextField 
                          variant='outlined'
                          label='Left'
                          onChange={e => handleChange({ breastAmounts: { ...formInfo.breastAmounts, left: e.target.value }})}
                          value={formInfo.breastAmounts ? formInfo.breastAmounts.left : '' || ''}
                          className={classes.textField}
                          type="number"
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <TextField 
                          variant='outlined'
                          label='Right'
                          onChange={e => handleChange({ breastAmounts: { ...formInfo.breastAmounts, right: e.target.value }})}
                          value={formInfo.breastAmounts ? formInfo.breastAmounts.right : '' || ''}
                          className={classes.textField}
                          type="number"
                        />
                      </Grid>
                    </div>
                  ) : null}
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={3}  className={classes.rows}>
            Note
            </Grid>
            <Grid item xs={9}  className={classes.rows}>
              <TextField 
                variant='outlined'
                multiline
                fullWidth
                value={formInfo.note}
                onChange={e => handleChange({note: e.target.value})}
                className={classes.textField}
              />
            </Grid>
          </Grid>
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
        <Button onClick={handleSubmit} disabled={IsSubmitting} color="primary">
          {IsSubmitting ? <Loader /> : 'Add'}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

EntryDialog.propTypes = {
  fnClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  editId: PropTypes.string,
}

EntryDialog.defaultProps = {
  editId: undefined
}

export default EntryDialog
