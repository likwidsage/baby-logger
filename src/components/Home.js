import React from 'react'
import Chart from './Chart'
import EntryTable from './EntryTable'
import Clock from './Clock'

function Home() {
  return (
    <>
      <Clock />
      <Chart />
      <EntryTable />
    </>
  )
}

export default Home
