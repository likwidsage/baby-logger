import React, {useState, useEffect} from 'react'
import { useSelector } from 'react-redux'
import { Typography, Paper } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(() => ({
  paper: {
    margin: 8,
    padding: 16
  }
}))

const Clock = () => {
  const entries = useSelector(store => store.entries.list)
  const lastTime = entries.filter(entry => entry.bottleAmount > 0)[0]
  const [TimeSince, SetTimeSince] = useState('999')
  const classes = useStyles()

  useEffect(() => {
    if(lastTime){
      let clock = setInterval(() => {
        let diff = (new Date() - new Date(lastTime.time))
        let hh = Math.floor(diff / 1000 / 60 / 60)
        let mm = Math.floor((diff / 1000 / 60) - (60 * hh)).toString().padStart(2,'0')
        let ss = Math.floor((diff / 1000) - (60*60 * hh) - (60 * mm)).toString().padStart(2,'0')
        SetTimeSince(`${hh}:${mm}:${ss}`)
      }, 1000)
      return () => clearInterval(clock)
    }
  }, [lastTime])

  return (
    <Paper className={classes.paper}>
      <Typography>
        {TimeSince} Since last feeding
      </Typography>
    </Paper>
  )
}

export default Clock
