import React, { useEffect, useState } from 'react'
import { Paper, Typography, IconButton, Dialog, DialogContent, DialogContentText, DialogActions, Button, Grid } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios'
import {SetEntries} from '../actions/entriesActions'

import Delete from '@material-ui/icons/Delete'
import Edit from '@material-ui/icons/Edit'
import Image from '@material-ui/icons/Image'
import EntryDialog from './dialogs/EntryDialog'
import shortid from 'shortid'

import { Table, AutoSizer } from 'react-virtualized'

import { Link } from 'react-router-dom'

const useStyles = makeStyles(() => ({
  paper: {
    margin: 8,
    height: 400,
    padding: 16,
    marginBottom: 72
  },
  header: {
    paddingBottom: 16
  },
  Cell: {
    padding: 10,
  },
  delete: {
    color: '#ff0000',
    padding: 5
  },
  edit: {
    color: '#898c00',
    padding: 5
  },
}))

const EntryTable = () => {
  const classes = useStyles()
  const dispatch = useDispatch()
  const entries = useSelector(state => state.entries.list)
  const [OpenEntry, setOpenEntry] = useState(false)
  const [editId, setEditId] = useState(undefined)
  const [Deleting, setDeleting] = useState(undefined)

  useEffect(() => {
    axios.get('/api/entries')
      .then(res => {
        dispatch(SetEntries(res.data))
      })
  }, [dispatch])

  const handleEdit = id => {
    console.log('Edit: ', id)
    setEditId(id)
    setOpenEntry(true)
  }

  const handleDelete = (id) => {
    axios.delete(`/api/entries/${id}`)
      .then(res => {
        console.log('Removed: ', res.data)
        axios.get('/api/entries')
          .then(res => {
            setDeleting(undefined)
            dispatch(SetEntries(res.data))
          })
      })
  }

  const formatTime = (time) => {
    return new Date(time).toLocaleString(
      'en-US', { timeZone: 'America/New_York', 
        weekday: 'short',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit' })
  }

  const rowRenderer = ({rowData, style}) => {
    const entry = rowData
    return (
      <Grid container style={style} key={shortid.generate()}>
        <Grid item xs={3}>
          <Typography variant='caption'>
            {formatTime(entry.time)}
          </Typography>
        </Grid>
        <Grid item xs={2}>
          {entry.diaperContents.poop ? '💩' : null}
          {entry.diaperContents.pee ? '💦' : null}
          {entry.diaperContents.image ? (
            <>
            <Link to={`/image/${entry.diaperContents.image}`}>
              <Image />
            </Link>
          </>
          ) : null}
        </Grid>
        <Grid item xs={2}>
          {entry.wasBottleFed ? `🍼 ${entry.bottleAmount}`: null}
          {entry.breastAmounts ? (
            <div>
              {entry.breastAmounts.left ? `L: ${entry.breastAmounts.left}` : null}
              {entry.breastAmounts.left ? ' | ': null } 
              {entry.breastAmounts.right ? `R: ${entry.breastAmounts.right}` : null}
            </div>
          ) : null}
        </Grid>
        <Grid item xs={3}>
          {entry.note}
        </Grid>
        <Grid item xs={2}>
          <IconButton className={classes.delete} onClick={() => setDeleting(entry._id)}>
            <Delete /> 
          </IconButton>
          <IconButton className={classes.edit} onClick={() => handleEdit(entry._id)}>
            <Edit /> 
          </IconButton>
        </Grid>
      </Grid>
    )
  }

  return (
    <Paper className={classes.paper}>
      <EntryDialog isOpen={OpenEntry} fnClose={() => {setOpenEntry(false)}} editId={editId} />
      <Dialog
        open={Deleting}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-desc  ription">
            Are you sure you want to delete?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setDeleting(undefined)} color="primary">
            Cancel
          </Button>
          <Button onClick={() => handleDelete(Deleting)} color="primary" autoFocus>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
      <Grid container className={classes.header}>
        <Grid item xs={3}>
          <Typography variant='body1'>
            Time
          </Typography>
        </Grid>
        <Grid item xs={2}>
          <Typography variant='body1'>
            Diaper
          </Typography>
        </Grid>
        <Grid item xs={2}>
          <Typography variant='body1'>
            Feeding
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography variant='body1'>
            Note
          </Typography>
        </Grid>
        <Grid item xs={2}>
          <Typography variant='body1'>
            Actions
          </Typography>
        </Grid>
      </Grid>
      <AutoSizer>
        {({ height, width }) => (
          <Table
            width={width}
            height={height-32}
            rowHeight={50}
            rowCount={entries.length}
            rowGetter={({ index }) => entries[index]}
            rowRenderer={rowRenderer}
            style={{width: '100%'}}
          />
        )}
      </AutoSizer>
    </Paper>
  )
}

export default EntryTable