import React, {useState} from 'react'
import {BrowserRouter} from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'

import { createStore, applyMiddleware } from 'redux'
import entries from './reducers'
import { composeWithDevTools } from 'redux-devtools-extension'
import { Provider } from 'react-redux'

import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'

import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'

import './App.css'
import { AppBar, Typography, Toolbar} from '@material-ui/core'
import MainRouter from './components/MainRouter'
import EntryDialog from './components/dialogs/EntryDialog'

const store = createStore( entries,composeWithDevTools(applyMiddleware()) )

const useStyles = makeStyles(theme => ({
  fab: {
    bottom: theme.spacing(1),
    right: theme.spacing(1),
    position: 'fixed'
  },
  paper: {
    margin: 8,
  },
  tableCell: {
    padding: 10,    
  },
  chart: {
    height: 500,
    margin: 8,
    marginBottom: 72
  },
  Appbar: {
    padding: 8
  }
}))

function App() {
  const classes = useStyles()
  const [openDiag, setOpenDiag] = useState(false)

  const handleAdd = () => {
    setOpenDiag(true)
    console.log('Add')
  }

  return (
    <BrowserRouter>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <Provider store={store}>
          <EntryDialog isOpen={openDiag} fnClose={() => setOpenDiag(false)} />
          <AppBar position="static">
            <Toolbar className={classes.Appbar}>
              <Typography variant="h5">
                Baby Logger
              </Typography>
            </Toolbar>
          </AppBar>

          <MainRouter />
        
          <Fab
            color="primary"
            aria-label="Add"
            className={classes.fab}
            onClick={handleAdd}
          >
            <AddIcon />
          </Fab>
        </Provider>
      </MuiPickersUtilsProvider>
    </BrowserRouter>
  )
}

export default App
