const config = {
  db_url: process.env.MongoURL || 'mongodb://localhost:27017/baby-logger',
  imageBasePath: process.env.imagePath || './images',
}

module.exports = config