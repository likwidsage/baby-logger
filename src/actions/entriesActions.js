import { SET_ENTRIES } from '../reducers/types'

export function SetEntries(entries){
  return {
    type: SET_ENTRIES,
    payload: entries
  }
}