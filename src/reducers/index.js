import { combineReducers } from 'redux'
import { entryReducer } from '../reducers/entryReducers'


export default combineReducers(
  {
    entries: entryReducer
  }
)