import { SET_ENTRIES } from './types'

const initState = {
  list: []
}

export const entryReducer = (state = initState, action) => {
  switch (action.type){
  case SET_ENTRIES:
    return {
      ...state,
      list: action.payload
    }
  default:
    return state
  }
}