const bodyParser = require('body-parser')
const express = require('express')
const app = express()
const cors = require('cors')
const mongoose = require('mongoose')
const {db_url, imageBasePath} = require('./src/conf/config')

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

mongoose
  .connect(db_url, {
    useNewUrlParser: true
  })
  .then(() => {
    console.log(`MongoDB connected: ${db_url}`)
  })
  .catch(() => {
    console.log(`Could not connect to mongo: ${db_url}`)
  })
 
const entries = require('./api/entriesApi')
app.use('/api/entries', entries)

app.use('/image', express.static(imageBasePath))

// app.use(express.static('build'))
// const path = require('path')
// app.get('*', (req, res) => {
//   res.sendFile(path.resolve(__dirname, 'build', 'index.html'))
// })

const port = 5000
app.listen(port, () => {
  console.log(`API on port ${port} in ${app.get('env')} mode`)
})
