const app = require('express')()
const multer = require('multer')
const { imageBasePath } = require('../src/conf/config')
const fs = require('fs')

const mongoose = require('mongoose')
const entries = require('../schemas/entriesSchema')
const ModelEntry = mongoose.model('Entry', entries)
const images = require('../schemas/imagesSchema')
const ModelImage = mongoose.model('Image', images)

const upload = multer({ dest: imageBasePath })

// TODO get user ID to use correct folder
const folderID = '1'

app.get('/', (req, res) => {
  ModelEntry.find()
    .sort({ time: -1 })
    .exec((err, arrDoc) => {
      console.log('get entries')
      if (err) {
        return res.status(400).json(err)
      }
      return res.status(200).json(arrDoc)
    })
})

app.get('/image/:id', (req, res) => {
  const { id } = req.params

  ModelImage.findById(id)
    .then(image => {
      return res.status(200).json(image)
    })
    .catch(err => {
      console.log(err)
      return res.status(400)
    })
})

app.post('/image', upload.single('imageData'), (req, res) => {
  console.log('Image uploading:', req.file)
  const nameSplit = req.file.originalname.split('.')
  const ext = nameSplit[nameSplit.length - 1]
  const srcFilePath = `${imageBasePath}/${req.file.filename}`
  const destFolder = `${imageBasePath}/${folderID}`
  const destFilePath = `${destFolder}/${req.file.filename}.${ext}`

  if (req.file) {
    fs.exists(destFolder, exists => {
      if (!exists) {
        fs.mkdir(destFolder, (err) => {
          if (err) {
            console.error(err)
            return res.status(500).json({ err })
          }
          console.log('Directory created for images.')

          fs.copyFile(srcFilePath, destFilePath, () => {
            const imageJson = {
              userid: 1,
              fileName: `${req.file.filename}.${ext}`,
              date: new Date()
            }

            let newImage = new ModelImage(imageJson)
            newImage.save((err, image) => {
              fs.unlinkSync(srcFilePath)
              return res.status(200).json({ image })
            })

          })

        })
      } else {

        fs.copyFile(srcFilePath, destFilePath, () => {
          const imageJson = {
            userid: 1,
            fileName: `${req.file.filename}.${ext}`,
            date: new Date()
          }

          let newImage = new ModelImage(imageJson)
          newImage.save((err, image) => {
            fs.unlinkSync(srcFilePath)
            return res.status(200).json({ image })
          })

        })

      }
    })
  }
})

app.delete('/image', (req, res) => {
  const { image } = req.body
  fs.unlink(`${imageBasePath}/${image}`, err => {
    if (err && err.code !== 'ENOENT') {
      console.log(err.code)
      return res.status(500).json(err)
    }
    return res.status(200).json({})
  })
})

app.delete('/image/:id', (req, res) => {
  const { id } = req.params
  ModelImage.findByIdAndDelete(id)
    .then(() => {
      return res.status(200)
    })
    .catch(err => {
      return res.status(400).json(err)
    })
})

app.post('/', (req, res) => {
  let entryJson = req.body

  console.log('save: ', entryJson)
  if (entryJson._id) {
    ModelEntry.findOneAndUpdate({ _id: entryJson._id }, entryJson, (err, doc2) => {
      console.log('File Deleted and Entry updated')
      return res.status(200).json(doc2)
    })
  } else {
    var newEntry = new ModelEntry(entryJson)
    newEntry.save((err, entry) => {
      console.log('Entry Saved')
      return res.status(200).json(entry)
    })
  }
})

app.delete('/:id', (req, res) => {
  var { id } = req.params
  ModelEntry.deleteOne({ _id: id }).then(() => {
    console.log(`Removed ${id}`)
    return res.status(200).json(id)
  })
})

module.exports = app
