var mongoose = require('mongoose')

var EntrySchema = new mongoose.Schema({
  userid: {type: Number},
  time: {type: Date, default: Date.now},
  diaperContents: {
    pee: { type: Boolean, default: false },
    poop: { type: Boolean, default: false },
    image: { type: String }
  },
  wasBreastFed: Boolean,
  breastAmounts: {
    left: Number,
    right: Number
  },
  wasBottleFed: Boolean,
  bottleAmount: Number,
  note: String
})

module.exports = EntrySchema
