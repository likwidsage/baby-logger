var mongoose = require('mongoose')

var ImageSchema = new mongoose.Schema({
  userid: {type: Number},
  fileName: {type: String},
  date: { type: Date }
})

module.exports = ImageSchema
